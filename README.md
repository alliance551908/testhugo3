| Commande | Description |
|----------|-------------|
| **Git global setup** | |
| git config --global user.name "meerak0le" | Configure le nom d'utilisateur Git globalement |
| git config --global user.email "plaquistes_cedres_0i@icloud.com" | Configure l'e-mail utilisateur Git globalement |
| **Créer un nouveau dépôt** | |
| git clone git@gitlab.com:alliance551908/dd.git | Clone un nouveau dépôt |
| cd dd | Change de répertoire dans le répertoire cloné |
| git switch --create main | Crée une nouvelle branche principale (main) |
| touch README.md | Crée un nouveau fichier README.md |
| git add README.md | Ajoute le fichier README.md pour le suivi |
| git commit -m "add README" | Valide les changements avec un message |
| git push --set-upstream origin main | Pousse les changements vers la branche principale sur le dépôt distant |
| **Pousser un dossier existant** | |
| cd existing_folder | Change de répertoire vers le dossier existant |
| git init --initial-branch=main | Initialise un dépôt Git avec une branche principale (main) |
| git remote add origin git@gitlab.com:alliance551908/dd.git | Ajoute l'URL du dépôt distant comme origine |
| git add . | Ajoute tous les fichiers et dossiers pour le suivi |
| git commit -m "Initial commit" | Valide les changements initiaux avec un message |
| git push --set-upstream origin main | Pousse les changements vers la branche principale sur le dépôt distant |
| **Pousser un dépôt Git existant** | |
| cd existing_repo | Change de répertoire vers le dépôt Git existant |
| git remote rename origin old-origin | Renomme l'origine existante (optionnel) |
| git remote add origin git@gitlab.com:alliance551908/dd.git | Ajoute l'URL du dépôt distant comme origine |
| git push --set-upstream origin --all | Pousse toutes les branches locales vers le dépôt distant |
| git push --set-upstream origin --tags | Pousse toutes les balises (tags) locales vers le dépôt distant |

# Git Commandes

Ce fichier README liste et décrit diverses commandes Git couramment utilisées.

| Commande | Description |
|----------|-------------|
| `git branch` | Liste toutes les branches dans le dépôt. La branche courante est marquée avec un astérisque (`*`). |
| `git branch -D <branch_name>` | Supprime de force la branche nommée `<branch_name>`, même si elle n'a pas été fusionnée. À utiliser avec précaution. |
| `git branch -M <new_name>` | Renomme la branche courante en `<new_name>`. |
| `git checkout -b <new_branch> <start_point>` | Crée une nouvelle branche nommée `<new_branch>` et bascule dessus, à partir de `<start_point>`. |
| `git checkout <branch_name>` | Bascule sur la branche nommée `<branch_name>`. |
| `git merge <branch_name>` | Fusionne la branche nommée `<branch_name>` dans la branche courante. |
| `git merge <branch_name> --allow-unrelated-histories` | Fusionne la branche nommée `<branch_name>` dans la branche courante, même si les branches n'ont pas d'historique commun. |
| `git rebase --continue` | Continue le processus de rebasage après avoir résolu les conflits. |
| `git rebase <branch_name>` | Rebase la branche courante sur `<branch_name>`, réappliquant vos changements sur `<branch_name>`. |
| `git log --oneline` | Affiche l'historique des commits de manière compacte, chaque commit sur une seule ligne. |
| `git log` | Affiche l'historique des commits avec des détails sur chaque commit, y compris le message, l'auteur, la date et le hash du commit. |
| `git remote show <remote_name>` | Affiche des informations détaillées sur le dépôt distant spécifié `<remote_name>`. |
| `git remote show` | Liste les dépôts distants actuellement configurés pour le dépôt. |
| `git fetch` | Télécharge les objets et les refs d'un autre dépôt, mettant à jour vos branches de suivi à distance sans modifier votre répertoire de travail ou la branche courante. |
| `git rm --cached <file>` | Supprime `<file>` de la zone de staging (index) mais le laisse dans le répertoire de travail. |
| `git rm -r --cached <directory>` | Supprime un répertoire `<directory>` et tout son contenu de la zone de staging mais les laisse dans le répertoire de travail. |
| `git stash` | Sauvegarde les modifications courantes dans votre répertoire de travail et votre index (zone de staging) pour une utilisation ultérieure, vous permettant de travailler avec un répertoire propre. |
| `git stash drop` | Supprime une entrée de stash de la liste des stashes. Si aucun stash n'est spécifié, supprime le plus récent. |

